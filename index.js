
const FIRST_NAME = "Andrada";
const LAST_NAME = "Alecu";
const GRUPA = "1090";

 
    function initCaching() {
        var cache={};
        cache.home=0;
        cache.about=0;
        cache.contact=0;

        cache.getCache=function(){
            return cache;
        }

        cache.pageAccessCounter=function(vari="HOME"){
            if(vari===undefined){
                this.home++; 
            }
             else{
                
                if(vari.toUpperCase()==="HOME"){
                    this.home++;
                    }
                if(vari.toUpperCase()==="ABOUT"){
                        this.about++;
                    }
                if(vari.toUpperCase()==="CONTACT"){
                    this.contact++;
                    }    
                }
        };
      
        return cache;
    }
 

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

